package failure;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestTypeFailures {
  
  @Test
  public void testPlaceholder() {
    //interp is not working right now
    assertTrue(true);
  }
  
  /*
  @Test
  public void testConstPrimitives() {
    String srcCode = "module a; fun int main() {const int x = 10;x = 5;}";
    
    try {
      CommandHub.interp(true, srcCode);
      //assertEquals("x is const and cannot be an lvalue", e.coreMsg);
    } catch (Exception e) {
      fail("Expected a TypeError");
    }
  }
  
  @Test
  public void testConstDot() {
    String srcCode = "module a;class Test {public {int x;init(int _x) {x = _x;"
        + "}}}fun int main() {const Test test = Test<>(15);test.x = 5;}";
    
    try {
      CommandHub.interp(true, srcCode);
      //assertEquals("left side of lvalue.id is const", e.coreMsg);
    } catch (Exception e) {
      fail("Expected a TypeError");
    }
  }
  
  @Test
  public void testConstBracket() {
    //test const array
    String srcCode = "module a;fun int main() {const [int] arr = [int][0,1,2];arr[2] = 5;}";
    
    try {
      CommandHub.interp(true, srcCode);
      fail("Expected a TypeError");
      //assertEquals("left side of lvalue[idx] is const", e.coreMsg);
    } catch (Exception e) {
      fail("Expected a TypeError");
    }
    
    //test array of const
    srcCode = "module a;fun int main() {[const int] arr = [int][0,1,2];arr[2] = 5;}";
    
    try {
      CommandHub.interp(true, srcCode);
      fail("Expected a TypeError");
      //assertEquals("lvalue[idx] is const: const int", e.coreMsg);
    } catch (Exception e) {
      fail("Expected a TypeError");
    }
    
    //test putting const into non-const
    srcCode = "module a;class Test {public {int x;init(int _x) {x = _x;"
        + "}}}fun int main() {const Test test = Test<>(15);"
        + "[Test*] arr = [const Test*][&test];}";
    
    try {
      CommandHub.interp(true, srcCode);
      //TODO come back to this
      //fail("interp passed, but expected a TypeError");
      //assertEquals("declaration assignment value [const Test*] cannot cast up to declared type [Test*]", e.coreMsg);
    } catch (Exception e) {
      e.printStackTrace();
      fail("Expected a TypeError");
    }
    
    //test making a class member const (functions are already const)
    srcCode = "module a;class Test {public {const int x;init(int _x) {x = _x;"
        + "}}}\n"
        + "fun int main() {Test test = Test<>(15);test.x = 5;}";
    
    try {
      CommandHub.interp(true, srcCode);
      fail("Expected a TypeError");
      //assertEquals("right side of lvalue.id is const", e.coreMsg);
      //assertEquals(2, e.lineNum);
    } catch (Exception e) {
      e.printStackTrace();
      fail("Expected a TypeError");
    }
  }

  @Test
  public void testReadWritePrivate() {
    //test write
    String srcCode = "module a;class Test {private {const int x;}public{init(int _x) {x = _x;"
        + "}}}\n"
        + "fun int main() {Test test = Test<>(15);test.x = 5;}";
    
    try {
      CommandHub.interp(true, srcCode);
      fail("Expected a TypeError");
      //assertEquals("Not within class [Test], cannot access field: x", e.coreMsg);
      //assertEquals(2, e.lineNum);
    } catch (Exception e) {
      e.printStackTrace();
      fail("Expected a TypeError");
    }
    
    //test read
    srcCode = "module a;class Test {private {const int x;}public{init(int _x) {x = _x;"
        + "}}}\n"
        + "fun int main() {Test test = Test<>(15);print(test.x + \"\\n\");}";
    
    try {
      CommandHub.interp(true, srcCode);
      fail("Expected a TypeError");
      //assertEquals("Not within class [Test], cannot access field: x", e.coreMsg);
      //assertEquals(2, e.lineNum);
    } catch (Exception e) {
      fail("Expected a TypeError");
    }
    
    //test write protected
    srcCode = "module a;class Test {protected {const int x;}public{init(int _x) {x = _x;"
        + "}}}\n"
        + "fun int main() {Test test = Test<>(15);test.x = 5;}";
    
    try {
      CommandHub.interp(true, srcCode);
      fail("Expected a TypeError");
      //assertEquals("Not within class [Test], cannot access field: x", e.coreMsg);
      //assertEquals(2, e.lineNum);
    } catch (Exception e) {
      fail("Expected a TypeError: " + e);
    }
    
    //test read protected
    srcCode = "module a;class Test {protected {const int x;}public{init(int _x) {x = _x;"
        + "}}}\n"
        + "fun int main() {Test test = Test<>(15);print(test.x + \"\\n\");}";
    
    try {
      CommandHub.interp(true, srcCode);
      fail("Expected a TypeError");
      //assertEquals("Not within class [Test], cannot access field: x", e.coreMsg);
      //assertEquals(2, e.lineNum);
    } catch (Exception e) {
      fail("Expected a TypeError");
    }
  }
  
  @Test
  public void testReadWriteProtected() {
    //test write other class's private
    String srcCode = "module a;class A {private {int x;}protected{int y = 5;}public{init() {x = 0;}}}\n"
        + "class B extends A {private {int z;}protected{int w = 2;}public{init(int _z) {z = _z;}}}\n"
        + "fun int main() {B test = B<>(15);test.x = 5;}";
    
    try {
      CommandHub.interp(true, srcCode);
      fail("Expected a TypeError");
      //assertEquals("Not within class [A], cannot access field: x", e.coreMsg);
    } catch (Exception e) {
      e.printStackTrace();
      fail("Expected a TypeError");
    }
    
    //test write other class's protected
    srcCode = "module a;class A {private {int x;}protected{int y = 5;}public{init() {x = 0;}}}\n"
        + "class B extends A {private {int z;}protected{int w = 2;}public{init(int _z) {z = _z;y = 17;x = 0;}}}\n"
        + "fun int main() {B test = B<>(15);}";
    
    try {
      CommandHub.interp(true, srcCode);
      fail("Expected a TypeError");
      //assertEquals("Cannot access private member in class [A] from class [B]: x", e.coreMsg);
    } catch (Exception e) {
      e.printStackTrace();
      fail("Expected a TypeError");
    }
  }
  */
}
