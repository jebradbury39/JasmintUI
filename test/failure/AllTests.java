package failure;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestAccessFailures.class, TestTypeFailures.class, TestRegressions.class })
public class AllTests {

}
