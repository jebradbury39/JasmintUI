package failure;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestAccessFailures {
  
  @Test
  public void testPlaceholder() {
    //interp is not working right now
    assertTrue(true);
  }
/*
  @Test
  public void testStaticGlobal() {
    //Test.y should fail if y is only found in GLOBAL
    String srcCode = "module a;class Test {public {static int z;int x;init(int _x) {x = _x;"
        + "}}}fun int main() {int y = 3;Test.z = 5;Test.y = 4;return 0;}";
    
    try {
      CommandHub.interp(true, srcCode);
      //assertEquals("<class declaration Test> does not have a field 'y'", e.coreMsg);
    } catch (Exception e) {
      fail("Expected a TypeError, got: " + e);
    }
  }
  
  @Test
  public void testInstanceGlobal() {
    //Test.y should fail if y is only found in GLOBAL
    String srcCode = "module a;class Test {public {static int z;int x;init(int _x) {x = _x;"
        + "}}}fun int main() {Test test = Test<>(5);test.x = 1;test.y = 2;return 0;}";
    
    try {
      CommandHub.interp(true, srcCode);
      //assertEquals("Test does not have a field 'y'", e.coreMsg);
    } catch (Exception e) {
      fail("Expected a TypeError");
    }
  }
*/
}
