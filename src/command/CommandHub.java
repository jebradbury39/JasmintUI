package command;

import ast.SerializationError;
import errors.FatalMessageException;
import errors.Nullable;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.NoSuchFileException;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;
import java.util.Scanner;
import main.Main;
import multifile.Project;
import multifile.ProjectAssembler;
import threading.ThreadManager;

public class CommandHub {
  private Project loadedProject = null;

  public static String interp(boolean onlyTypecheck, String srcCode) throws Exception {
    // redirect stdout for the duration of this test
    OutputStream outStream = new ByteArrayOutputStream();
    PrintStream captureOut = new PrintStream(outStream);
    PrintStream stdout = System.out;
    PrintStream stderr = System.err;
    System.setOut(captureOut);
    System.setErr(captureOut);

    // parse and run, if not only typechecking
    Project loadedProgram = ProjectAssembler.assembleSimpleProject(srcCode, true);

    loadedProgram.typecheck();

    if (!onlyTypecheck) {
      // loadedProgram.interp();
    }

    // reset stdout
    System.setOut(stdout);
    System.setErr(stderr);
    String testStdout = outStream.toString();

    return testStdout;
  }

  public void expressExecute(String fname, boolean _serialize, String _deserialize) {
    if (_deserialize != null) {
      commandDeserialize(_deserialize);
    } else {
      try {
        if (fname.endsWith(".jsmnt-build")) {
          ProjectAssembler pa = new ProjectAssembler(fname, Nullable.empty());
          Optional<Project> tmp = pa.assemble();
          if (tmp.isPresent()) {
            loadedProject = tmp.get();
          } else {
            error(pa.printErrors());
          }
        } else {
          loadedProject = ProjectAssembler.assembleSimpleProject(fname, false);
        }
      } catch (NoSuchFileException e) {
        error("Failed to load file [" + fname + "] since this file does not exist.");
      } catch (Exception e) {
        e.printStackTrace();
        error("Failed to load file [" + fname + "]. Reason: " + e.getMessage());
      }
    }

    if (loadedProject.hitError()) {
      error(loadedProject.errorReport());
    }

    ThreadManager threadManager = new ThreadManager(1);

    try {
      try {
        boolean linked = loadedProject.linkModules(threadManager);
        threadManager.stop();
        if (linked) {
          loadedProject.typecheck();
        }
        if (!loadedProject.hitError()) {
          loadedProject = loadedProject.resolveUserTypes().get();
        }
      } catch (FatalMessageException e1) {
        e1.printStackTrace();
        System.out.println(loadedProject.errorReport());
        threadManager.stop();
        return;
      } catch (Exception e) {
        e.printStackTrace();
        threadManager.stop();
        return;
      }
    } catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return;
    }
    if (loadedProject.hitError()) {
      error(loadedProject.errorReport());
    }

    if (!Main._silentRunning) {
      System.out.println("Loaded and typechecked");
    }

    if (!_serialize) {
      // loadedProject.interpMain();
      if (!Main._silentRunning) {
        System.out.println("\nFinished execution");
      }
    } else {
      commandSerialize("");
    }
  }

  public void handleCommand(String cmd) {
    switch (cmd) {
      case "load":
        commandLoad();
        break;
      case "load_project":
        commandLoadProject();
        break;
      case "execute":
        commandExecute();
        break;
      case "serialize":
        commandSerialize(null);
        break;
      case "deserialize":
        commandDeserialize(null);
        break;
      case "?":
      default:
        showHelp();
    }
  }

  private void showHelp() {
    System.out.println("Commands:\n" + " - load\n" + " - load_project\n" + " - execute\n"
        + " - serialize\n" + " - deserialize\n" + " - ?");
  }

  @SuppressWarnings("resource")
  private void commandLoad() {
    System.out.print("File to load: ");
    Scanner scanner = new Scanner(System.in);
    String input = scanner.nextLine().trim();

    try {
      loadedProject = ProjectAssembler.assembleSimpleProject(input, false);
    } catch (Exception e) {
      e.printStackTrace();
      System.out.println("Failed to load file [" + input + "]. Reason: " + e.getMessage());
      return;
    }

    try {
      loadedProject.typecheck();
    } catch (FatalMessageException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    System.out.println("Loaded and typechecked");

  }

  @SuppressWarnings("resource")
  private void commandLoadProject() {
    System.out.print("Project config file (*.json): ");
    Scanner scanner = new Scanner(System.in);
    String input = scanner.nextLine().trim();

    ProjectAssembler assembler = new ProjectAssembler(input, Nullable.empty());
    try {
      loadedProject = assembler.assemble().get();
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      loadedProject.typecheck();
    } catch (FatalMessageException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    System.out.println("Loaded and typechecked project");

  }

  public static void error(String msg) {
    System.err.println("JasmintUI error: " + msg);
    System.exit(1);
  }

  private void commandExecute() {
    // loadedProject.interpMain();
    System.out.println("\nFinished execution");
  }

  @SuppressWarnings("resource")
  private void commandSerialize(String input) {
    if (loadedProject == null) {
      System.out.println("cannot serialize null program");
      return;
    }

    boolean toJson = false;
    if (input == null) {
      System.out.print("Serialize to file: ");
      Scanner scanner = new Scanner(System.in);
      input = scanner.nextLine().trim();

      System.out.print("use json? [y/n]: ");
      toJson = scanner.nextLine().trim().startsWith("y");
    } else {
      try {
        loadedProject.serializeToFile(true);
      } catch (IOException | SerializationError | NoSuchAlgorithmException
          | FatalMessageException e) {
        e.printStackTrace();
        System.err.println("unable to serialize loadedProgram");
      }
      if (!Main._silentRunning) {
        System.out.println("Successfully serialized program (json)");
      }
    }

    try {
      loadedProject.serializeToFile(toJson);
      if (!Main._silentRunning) {
        System.out.println("Successfully serialized program");
      }
    } catch (IOException | SerializationError | NoSuchAlgorithmException
        | FatalMessageException e) {
      e.printStackTrace();
      System.err.println("unable to serialize loadedProgram");
    }
  }

  @SuppressWarnings("resource")
  private void commandDeserialize(String input) {
    if (input == null) {
      System.out.print("Deserialize from file: ");
      Scanner scanner = new Scanner(System.in);
      input = scanner.nextLine().trim();
    }

    try {
      loadedProject = Project.deserializeFromFile(input, "build-jsmnt-ui");
      if (!Main._silentRunning) {
        System.out.println("Successfully deserialized program from file '" + input + "'");
      }
      loadedProject.typecheck();
    } catch (IOException ioe) {
      ioe.printStackTrace();
    } catch (SerializationError se) {
      se.printStackTrace();
    } catch (FatalMessageException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
}
