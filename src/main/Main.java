package main;

import command.CommandHub;
import errors.Message;
import java.util.Scanner;

public class Main {

  private static String _deserialize = null; // -d <input_file.bson> option, cannot use with -i
                                             // option
  private static boolean _serialize = false; // -s option, outputs via project assembler config
  private static String _interp = null; // -i <input_file.jsmnt> option
  public static boolean _silentRunning = false; // -r option, if true, show no info

  @SuppressWarnings("resource")
  public static void main(String[] args) {
    parseParameters(args);

    CommandHub commandHub = new CommandHub();

    if (_interp != null || _deserialize != null) {
      commandHub.expressExecute(_interp, _serialize, _deserialize);
      return;
    }

    Scanner scanner = new Scanner(System.in);
    while (true) {
      System.out.print("Enter a command: ");
      String input = scanner.nextLine().trim().toLowerCase();
      if ("quit".equals(input))
        break;
      commandHub.handleCommand(input);
    }
  }

  private static void parseParameters(String[] args) {
    for (int i = 0; i < args.length; i++) {
      if (args[i].charAt(0) == '-') {
        switch (args[i]) {
          case "-s":
            _serialize = true;
            break;
          case "-d":
            if (i < args.length - 1) {
              _deserialize = args[++i];
            }
            break;
          case "-i":
            if (i < args.length - 1) {
              _interp = args[++i];
            }
            break;
          case "-r":
            _silentRunning = true;
            break;
          case "-e":
            Message.DebugMode = false;
            break;
          default:
            System.err.println("unexpected option: " + args[i]);
            System.exit(1);
            break;
        }
      } else {
        System.err.println("Invalid argument: " + args[i]);
        System.exit(1);
      }
    }
  }
}
