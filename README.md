# JasmintUI

This project creates an executable to provide default access to the Jasmint library.

### Building

1. Build the Jasmint library
2. Copy `jasmint.jar` into `lib/`
3. Run `ant`
4. Run `java -jar dist/jasmint-ui.jar`

### Usage

First off, this interface could definitely use improvement. That said...
On start, the user is given a prompt to enter a command. This is one of:

* `load` - Reads a `.jsmnt` file and runs the typechecker on it
* `execute` - Run the interpreter on the file and output results. Cannot interp a file with a trans block at this time
* `serialize` - This gives another prompt to enter a `.bson` filename to save to
* `deserialize` - This gives another prompt to enter a `.bson` filename to read from
* `?` - List all commands

If a filename is provided on the initial run e.g. `jasmint-ui.jar <filename>`, then that file is loaded and executed. JasmintUI then terminates.