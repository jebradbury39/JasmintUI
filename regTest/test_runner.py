from typing import *

import os
import sys
import glob
import re
import traceback
import pdb
import json
import logging
from logging.handlers import RotatingFileHandler
from pathlib import Path
import fnmatch
import time

def initLogging() -> Any:
   log = logging.getLogger('test-run')
   log.setLevel(logging.DEBUG)
   fh = RotatingFileHandler('test-run.log', backupCount=3)
   fh.doRollover()
   fh.setLevel(logging.DEBUG)

   sh = logging.StreamHandler()
   sh.setLevel(logging.DEBUG)

   formatter = logging.Formatter('%(asctime)s,%(msecs)d %(levelname)s %(message)s', '%H:%M:%S')
   fh.setFormatter(formatter)
   sh.setFormatter(formatter)

   log.addHandler(fh)
   log.addHandler(sh)

   return log

log = initLogging()

parentdir = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0,parentdir) 

from process_handler import ProcessHandler, bToStr

# this class runs both test files and test projects
class TestRunner:
   def __init__(self, jacocoPath: str, pythonType: str, args: Any) -> None:
      self.jacocoArgs: str = '-javaagent:' + jacocoPath + '=destfile=' + args.ws + '/build/jacoco/test.exec,append=true'
      self.jasmintUI: str = '../JasmintUI/build/libs/JasmintUI-0.1.jar'
      self.jasmintCXX: str = '../JasmintCxxTranspiler/build/libs/JasmintCxxTranspiler-0.1.jar'
      self.jasmintCsharp: str = '../JasmintCsharpTranspiler/build/libs/JasmintCsharpTranspiler-0.1.jar'
      self.jasmintPy: str = '../JasmintPythonTranspiler/build/libs/JasmintPythonTranspiler-0.1.jar'

      self.testFileRunner = TestFileRunner(pythonType, self.jacocoArgs, self.jasmintUI,
         self.jasmintCXX, self.jasmintCsharp, self.jasmintPy, args.test_files)
      self.testProjectRunner = TestProjectRunner(pythonType, self.jacocoArgs, self.jasmintUI,
         self.jasmintCXX, self.jasmintCsharp, self.jasmintPy, args.test_projects)

   def test(self) -> bool:
      startTime: float = time.time()

      filePass, fileFail = self.testFileRunner.test()
      projPass, projFail = self.testProjectRunner.test()

      # check if all passed. If not, display failures
      if fileFail:
         log.info(json.dumps(fileFail, indent=4, sort_keys=True))
      if projFail:
         log.info(json.dumps(projFail, indent=4, sort_keys=True))

      log.info('Testing Completed in {} sec'.format(round(time.time() - startTime, 2)))
      if not fileFail and not projFail:
         log.info('All pass!')
         return True
      totalPass: int = len(filePass) + len(projPass)
      totalFail: int = len(fileFail) + len(projFail)
      total: int = totalPass + totalFail

      log.info('FAILED: {}, PASSED: {}, TOTAL: {}'.format(totalFail, totalPass, total))
      return False
   

def stdExe(cmd: str) -> str:
   cmd += ' > cmd.out 2> cmd.err'
   log.info(cmd)
   rc = os.system(cmd)
   with open('cmd.err', 'r') as f:
      stderr = f.read()
   return stderr

def rmFiles(pattern: str) -> str:
   files = glob.glob(pattern)
   for f in files:
      try:
         os.remove(f)
      except Exception as e:
         return 'failed to remove "{}": {}'.format(f, e)
   return ''

# collect test files and put each one into 
class TestFileRunner:
   def __init__(self,
                pythonType: str,
                jacocoArgs: str,
                jasmintUI: str,
                jasmintCXX: str,
                jasmintCsharp: str,
                jasmintPy: str,
                testFileDirs: List[str]) -> None:
      self.pythonType = pythonType
      self.jacocoArgs = jacocoArgs
      self.jasmintUI = os.path.abspath(jasmintUI)
      self.jasmintUIDir = os.path.dirname(os.path.dirname(os.path.dirname(self.jasmintUI)))
      self.jasmintCXX = os.path.abspath(jasmintCXX)
      self.jasmintCxxDir = os.path.dirname(os.path.dirname(os.path.dirname(self.jasmintCXX)))
      self.jasmintCsharp = os.path.abspath(jasmintCsharp)
      self.jasmintCsharpDir = os.path.dirname(os.path.dirname(os.path.dirname(self.jasmintCsharp)))
      self.jasmintPy = os.path.abspath(jasmintPy)
      self.jasmintPyDir = os.path.dirname(os.path.dirname(os.path.dirname(self.jasmintPy)))

      self.testFileDirs: List[str] = []
      self.testFiles: Dict[str, List[str]] = {} # map fname -> [ioFnames]
      for testDir in testFileDirs:
         testDir = os.path.abspath(testDir)
         self.testFileDirs.append(testDir)
         # find which dirs contain the single files
         testIter = Path(testDir).rglob('*.jsmnt')
         test: List[str] = [str(x.resolve()) for x in testIter]
         # get a set of only those dirs
         testSubDirSet: Set[str] = set()
         for subdir in test:
            testSubDirSet.add(os.path.dirname(subdir))

         # now run an iglob and make sure we only find one *jsmnt file in this dir
         for subdir in testSubDirSet:
            subdirIter = glob.iglob(os.path.join(subdir, '*.jsmnt'))
            subdirList: List[str] = [x for x in subdirIter]
            if len(subdirList) != 1:
               continue
            # directory which contains jsmnt+io files

            ioFilesIter = glob.iglob(os.path.join(subdir, '*.io'))
            ioFiles: List[str] = [x for x in ioFilesIter]
            if len(ioFiles) < 1:
               continue
            self.testFiles[subdirList[0]] = ioFiles

   def test(self) -> Tuple[Dict[str, Any], Dict[str, Any]]:
      log.info('Test file runner found tests ({}): {}'.format(
            self.testFileDirs, self.testFiles))
      passes: Dict[str, Any] = {}
      failures: Dict[str, Any] = {}
      for testFile, ioFiles in self.testFiles.items():
         log.info('TEST FILE ' + testFile)
         # test jasmint interp
         #p, f = TestExe("java " + self.jacocoArgs + " -jar " + self.jasmintUI + " -r -i " + testFile, ioFiles).test()
         #if f:
         #   failures[testFile] = ('During jasmint interp', f)
         #   continue

         # serialize to bson
         stderr = stdExe("java " + self.jacocoArgs + " -jar " + self.jasmintUI + " -r -i " + testFile + " -s")
         if stderr:
            failures[testFile] = ('Failed to serialize to bson: ' + stderr, None)
            continue

         # deserialize from bson TODO and interp again
         stderr = stdExe("java " + self.jacocoArgs + " -jar " + self.jasmintUI + " -r -d dist/project.jsmnt-project.gpb")
         if stderr:
            failures[testFile] = ('During jasmint interp from bson: ' + stderr, None)
            continue
        
         # transpile to python
         os.system('rm -rf build-py')
         os.system('mkdir build-py')
         os.chdir('build-py')
         projDir = self.jasmintUIDir
         stderr = stdExe('java -jar ' + self.jasmintPy + ' -builtin ' + os.path.join(self.jasmintPyDir, 'jsmnt_python_builtin/builtin.py') + ' ' + os.path.join(projDir, 'dist/project.jsmnt-project.gpb'))
         if stderr:
            failures[testFile] = ('Failed to transpile to python: ' + stderr, None)
            os.chdir('..')
            continue
         os.chdir('build-py')
         
         # test python exe
         p, f = TestExe(self.pythonType + ' main.py', ioFiles).test()
         if f:
            failures[testFile] = ('During python exe', f)
            os.chdir('../..')
            continue

         os.chdir('../..')
         
         # transpile to cxx
         os.system('rm -rf build-cxx')
         os.system('mkdir build-cxx')
         os.chdir('build-cxx')
         stderr = stdExe('java -jar ' + self.jasmintCXX + ' ' + os.path.join(projDir, 'dist/project.jsmnt-project.gpb'))
         if stderr:
            failures[testFile] = ('Failed to convert bson to cxx', None)
            os.chdir('..')
            continue
         os.chdir('build-cxx')
         stderr = stdExe('g++ */*.cpp -o a.out -std=c++11')
         if stderr:
            failures[testFile] = ('Failed to compile cxx: ' + stderr, None)
            os.chdir('../..')
            continue

         # test cxx exe
         p, f = TestExe('./a.out', ioFiles).test()
         if f:
            failures[testFile] = ('During c++ exe', f)
            os.chdir('../..')
            continue
         
         os.chdir('../..')

         # transpile to csharp
         os.system('rm -rf build-csharp')
         os.system('mkdir build-csharp')
         os.chdir('build-csharp')
         stderr = stdExe('java -jar ' + self.jasmintCsharp + ' ' + os.path.join(projDir, 'dist/project.jsmnt-project.gpb'))
         if stderr:
            failures[testFile] = ('Failed to convert bson to csharp', None)
            os.chdir('..')
            continue
         os.chdir('build-csharp')
         stderr = stdExe('csc */*.cs -out:a.exe')
         if stderr:
            failures[testFile] = ('Failed to compile csharp: ' + stderr, None)
            os.chdir('../..')
            continue
         stderr = stdExe('chmod +x a.exe')
         if stderr:
            failures[testFile] = ('Failed to make executable: ' + stderr, None)
            os.chdir('../..')
            continue

         # test cxx exe
         p, f = TestExe('./a.exe', ioFiles).test()
         if f:
            failures[testFile] = ('During csharp exe', f)
            os.chdir('../..')
            continue
         
         os.chdir('../..')
 
         passes[testFile] = (None, None)

      return passes, failures

def findFiles(directory, pattern):
   matches = []
   for root, dirs, files in os.walk(directory, topdown=True):
      for basename in files:
         if fnmatch.fnmatch(basename, pattern):
            filename = os.path.join(root, basename)
            matches.append(filename)
            # found a match in this tree, don't go further down
            dirs[:] = []
            break
   return matches

class TestProjectRunner:
   def __init__(self,
                pythonType: str,
                jacocoArgs: str,
                jasmintUI: str,
                jasmintCXX: str,
                jasmintCsharp: str,
                jasmintPy: str,
                testProjDirs: List[str]) -> None:
      self.pythonType = pythonType
      self.jacocoArgs = jacocoArgs
      self.jasmintUI = os.path.abspath(jasmintUI)
      self.jasmintCXX = os.path.abspath(jasmintCXX)
      self.jasmintCxxDir = os.path.dirname(os.path.dirname(os.path.dirname(self.jasmintCXX)))
      self.jasmintCsharp = os.path.abspath(jasmintCsharp)
      self.jasmintCsharpDir = os.path.dirname(os.path.dirname(os.path.dirname(self.jasmintCsharp)))
      self.jasmintPy = os.path.abspath(jasmintPy)
      self.jasmintPyDir = os.path.dirname(os.path.dirname(os.path.dirname(self.jasmintPy)))
      
      self.testProjDirs = testProjDirs
      self.testProjects: Dict[str, List[str]] = {}
      for pDir in testProjDirs:
         pDir = os.path.abspath(pDir)
         
         testIter = findFiles(pDir, '*.jsmnt-build')
         for buildfile in testIter:
            ioFilesIter = glob.iglob(os.path.join(os.path.dirname(buildfile), 'run_tests/*.io'))
            ioFiles = [x for x in ioFilesIter]
            if len(ioFiles) < 1:
               continue
            self.testProjects[buildfile] = ioFiles
   
   def test(self) -> Tuple[Dict[str, Any], Dict[str, Any]]:
      log.info('Test project runner found tests ({}): {}'.format(
         self.testProjDirs, self.testProjects))
      passes: Dict[str, Any] = {}
      failures: Dict[str, Any] = {}
      for testProj, ioFiles in self.testProjects.items():
         # compile the project to bson
         stderr = stdExe('java -jar ' + self.jasmintUI + ' -i ' + testProj + ' -s')
         if stderr:
            failures[testProj] = ('Failed to compile to bson: ' + stderr, None)
            continue
         
         # transpile to python
         os.system('rm -rf build-py')
         os.system('mkdir build-py')
         os.chdir('build-py')
         projDir = os.path.dirname(testProj)
         stderr = stdExe('java -jar ' + self.jasmintPy + ' -builtin ' + os.path.join(self.jasmintPyDir, 'jsmnt_python_builtin/builtin.py') + ' ' + os.path.join(projDir, 'dist/project.jsmnt-project.gpb'))
         if stderr:
            failures[testProj] = ('Failed to transpile to python: ' + stderr, None)
            os.chdir('..')
            continue
         os.chdir('build-py')
         
         # test python exe
         p, f = TestExe(self.pythonType + ' main.py', ioFiles).test()
         if f:
            failures[testProj] = ('During python exe', f)
            os.chdir('../..')
            continue

         os.chdir('../..')
         
         # transpile to cxx
         os.system('rm -rf build-cxx')
         os.system('mkdir build-cxx')
         os.chdir('build-cxx')
         stderr = stdExe('java -jar ' + self.jasmintCXX + ' ' + os.path.join(projDir, 'dist/project.jsmnt-project.gpb'))
         if stderr:
            failures[testProj] = ('Failed to convert bson to cxx', None)
            os.chdir('..')
            continue
         os.chdir('build-cxx')
         stderr = stdExe('g++ */*.cpp -o a.out -std=c++11')
         if stderr:
            failures[testProj] = ('Failed to compile cxx: ' + stderr, None)
            os.chdir('../..')
            continue

         # test cxx exe
         p, f = TestExe('./a.out', ioFiles).test()
         if f:
            failures[testProj] = ('During c++ exe', f)
            os.chdir('../..')
            continue
         
         os.chdir('../..')

         # transpile to csharp
         os.system('rm -rf build-csharp')
         os.system('mkdir build-csharp')
         os.chdir('build-csharp')
         stderr = stdExe('java -jar ' + self.jasmintCsharp + ' ' + os.path.join(projDir, 'dist/project.jsmnt-project.gpb'))
         if stderr:
            failures[testProj] = ('Failed to convert bson to csharp', None)
            os.chdir('..')
            continue
         os.chdir('build-csharp')
         stderr = stdExe('csc */*.cs -out:a.exe')
         if stderr:
            failures[testProj] = ('Failed to compile csharp: ' + stderr, None)
            os.chdir('../..')
            continue
         stderr = stdExe('chmod +x a.exe')
         if stderr:
            failures[testProj] = ('Failed to make executable: ' + stderr, None)
            os.chdir('../..')
            continue

         # test csharp exe
         p, f = TestExe('./a.exe', ioFiles).test()
         if f:
            failures[testProj] = ('During csharp exe', f)
            os.chdir('../..')
            continue
         
         os.chdir('../..')
         
         passes[testProj] = (None, None)

      return passes, failures

class TestExe:
   def __init__(self, exe: str, ioFiles: List[str]) -> None:
      self.exe: str = exe # full path to exe
      self.ioFiles: List[str] = ioFiles # full path to io files
      self.captureRegex = re.compile(r'\$CAPTURE\[(\d+)\]')

   def interpScript(self, proc: ProcessHandler, scriptLines: str) -> str:
      def tsend(data: str) -> None:
         log.info('  SCRIPT SEND: "{}"'.format(data))
         proc.send(data)

      def trecv(regex: str, timeout: float=10) -> Tuple[str, bool, Optional['Match']]:
         recv, success, match = proc.recv(regex, timeout=timeout)
         log.info('  SCRIPT RECV: {}" -> "{}"'.format(regex, bToStr(recv).replace('\n', '\\n').replace('\r', '\\r')))
         return bToStr(recv), success, match

      try:
         exec(scriptLines)
      except Exception:
         return traceback.format_exc()

      return ''

   def test(self) -> Tuple[Dict[str, Any], Dict[str, Any]]:
      passes: Dict[str, Any] = {}
      failures: Dict[str, Any] = {}
      indent = '  '
      for ioFile in self.ioFiles:
         startTime: float = time.time()
         log.info(indent + 'TESTING {} with {}'.format(self.exe, ioFile))
         with open(ioFile, 'r') as f:
            lines = [x for x in f.read().splitlines() if x.strip()]

         argvTmp = self.exe
         i = 0
         if lines[i] == '[ARGV]':
            argvTmp += ' ' + lines[1]
            i += 2
         argv: List[str] = [x for x in argvTmp.split() if x.strip()]

         # start exe via process handler
         cwd = os.getcwd()
         log.info('  Starting process {} in cwd="{}"'.format(argv, cwd))
         proc = ProcessHandler(argv, cwd=cwd)
         proc.start()
         log.info('    Process started: {}'.format(proc))
         # iterate over the remainder of the lines and follow instructions
         mode = None
         modes = {
            '[IN]': 'in',
            '[OUT]': 'out',
            '[OUT-REGEX]': 'out-regex',
            '[SCRIPT]': 'script'
         }
         rc = 0 # expected rc
         rcOp = '='
         recvMatch = None
         fail = False
         scriptLines = ''
         err = ''
         for i in range(i, len(lines)):
            line: str = lines[i]
            if line in modes:
               if scriptLines:
                  err = self.interpScript(proc, scriptLines)
                  if err:
                     failures[ioFile] = err
                     fail = True
                     break
                  scriptLines = ''
               mode = modes[line]
               continue
            
            m = re.match(r'\[RC(!=|=)(\d+)\]', line)
            if m:
               rcOp = m.group(1)
               rc = int(m.group(2))
               continue
            
            try:
               if mode == 'in':
                  line = line.replace('^C', '\003')
                  noNewline = line.endswith('[NEOL]')
                  if not noNewline:
                     line += '\n'

                  tmpLine = ''
                  lastIdx = 0
                  for m in self.captureRegex.finditer(line):
                     if not recvMatch:
                        raise Exception('no recv match found')
                     tmpLine += line[lastIdx:m.start()] + recvMatch.group(int(m.group(1)))
                     lastIdx = m.end()
                  tmpLine += line[lastIdx:]
                  line = tmpLine

                  proc.send(line)
                  log.info('  SEND: {}'.format(line))
               elif mode in ['out', 'out-regex']:
                  expectLine = line
                  if mode != 'out-regex':
                     line = re.escape(line) + '(\r\n|\r|\n|\Z)'
                  tmp_recv, success, _ = proc.recv(line, timeout=10)
                  recv: str = bToStr(tmp_recv)
                  log.info('  RECV({}): "{}" -> "{}"'.format(success, expectLine, recv))
                  if not success:
                     # attempt to recv everything else
                     try:
                        tmp_recv2, success, _ = proc.recv(r'(?!x)x', timeout=1)
                        recv2 = bToStr(tmp_recv2)
                     except Exception as e:
                        recv2 = str(e)
                     failures[ioFile] = 'failed to receive EXPECTED: "{}", but GOT: "{}" REMAINDER: "{}"'.format(expectLine, recv, recv2)
                     break
                  recvMatch = re.search(line, recv, re.MULTILINE)
                  if not recvMatch:
                     raise Exception('failed to find line: "{}" in "{}"'.format(line, recv))
               elif mode == 'script':
                  scriptLines += line + '\n'
            except:
               failures[ioFile] = traceback.format_exc()
               fail = True
               break

         if scriptLines:
            err = self.interpScript(proc, scriptLines)
            if err:
               failures[ioFile] = err
               fail = True
            scriptLines = ''

         log.info(indent + '  DONE ({} sec)'.format(round(time.time() - startTime, 2)))
         proc.end()
         if not fail:
            passes[ioFile] = 'PASS'
      return passes, failures
