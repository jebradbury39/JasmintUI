#! python

import os

def buildAllJasmint():
   # build jasmint lib
   print("building JasmintCsharpTranspiler", flush=True)
   os.chdir("../JasmintCsharpTranspiler/")
   os.system('gradle clean')
   os.system("gradle fatJar")
   print("building JasmintCxxTranspiler", flush=True)
   os.chdir("../JasmintCxxTranspiler/")
   os.system('gradle clean')
   os.system("gradle fatJar")
   print("building JasmintPythonTranspiler", flush=True)
   os.chdir("../JasmintPythonTranspiler/")
   os.system('gradle clean')
   os.system("gradle fatJar")
   print("building JasmintUI", flush=True)
   os.chdir("../JasmintUI/")
   os.system('gradle clean')
   os.system('gradle build')
   os.system("gradle fatJar")
   print("building fatJar", flush=True)

if __name__ == '__main__':
    buildAllJasmint()
