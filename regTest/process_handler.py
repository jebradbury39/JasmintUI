from typing import *

import os
import subprocess

isLinux: bool = True
try:
   import pty
   import termios
   import tty
   import fcntl
except Exception:
   isLinux = False
   import msvcrt

   from ctypes import windll, byref, wintypes, GetLastError, WinError
   from ctypes.wintypes import HANDLE, DWORD, LPDWORD, BOOL

   PIPE_NOWAIT = wintypes.DWORD(0x00000001)
   ERROR_NO_DATA = 232

from threading import Thread, Lock, Condition
import re
import time
import traceback
import pdb

class ProcessHandlerException(Exception):
   def __init__(self, msg: str, stopped: str) -> None:
      super().__init__("ProcessHandler (stopped={}) hit an error: {}".format(stopped, msg))
      self.stopped = stopped
      self.baseMsg = msg

def fdNonBlocking(fd):
   if isLinux:
      flag = fcntl.fcntl(fd, fcntl.F_GETFL)
      fcntl.fcntl(fd, fcntl.F_SETFL, flag | os.O_NONBLOCK)
      return True
   else:
     SetNamedPipeHandleState = windll.kernel32.SetNamedPipeHandleState
     SetNamedPipeHandleState.argtypes = [HANDLE, LPDWORD, LPDWORD, LPDWORD]
     SetNamedPipeHandleState.restype = BOOL

     h = msvcrt.get_osfhandle(fd)

     res = windll.kernel32.SetNamedPipeHandleState(h, byref(PIPE_NOWAIT), None, None)
     if res == 0:
        print(WinError())
        return False
     return True

def bToStr(b: Union[bytes, str]) -> str:
   if isinstance(b, bytes):
      return b.decode('ascii', 'ignore')
   return str(b).encode('ascii', 'ignore').decode('ascii', 'ignore')

def strToBytes(s: Union[str, bytes]) -> bytes:
   if isinstance(s, bytes):
      return s
   return str(s).encode('ascii', 'ignore')

# return rc, output, timeoutExpired, hitMatch
def runProcess(cmd: List[str],
               cwd: str='',
               pattern: str=r'',
               timeout: float=60) -> Tuple[int, bytes, bool, bool]:
   proc = ProcessHandler(cmd, cwd)

   rc = -1
   output = b''
   hitMatch = False
   timeoutExpired = False

   proc.start()
   startTime = time.time()

   if pattern:
      try:
         output, hitMatch, _ = proc.recv(pattern, timeout=timeout)
      except ProcessHandlerException as e:
         if e.stopped:
            output, rc = proc.end()
         else:
            raise

   elapsedTime = time.time() - startTime
   if hitMatch:
      ignOutput, rc = proc.end()
   elif elapsedTime >= timeout:
      tmp, rc = proc.end()
      output += tmp
      timeoutExpired = True
   else:
      output, timeoutExpired, rc = proc.waitForEnd(timeout=(timeout - elapsedTime))

   return rc, output, timeoutExpired, hitMatch

# This class allows for manipulating a process via stdin/stdout+stderr
# We can send/recv strings, so works best with ascii
class ProcessHandler:
   def __init__(self, command: List[str], cwd: str=os.getcwd(), bufSize: int=4096, numBuffers: int=100) -> None:
      self.bufSize: int = bufSize
      self.targetBufSize: int = bufSize * numBuffers
      self.command: List[str] = command
      self.cwd: str = cwd
      self.readOutFd: int = 0
      self.writeInFd: int = 0
      self.proc: Any = None
      
      self.recvBufferLock: Lock = Lock()
      self.recvBuffer: bytes = b''
      self.recvThread: Optional[Thread] = None
      self.doneReadingLock: Lock = Lock()
      self.doneRecvCondition: Condition = Condition(self.doneReadingLock)
      self.doneReading: bool = False      

      self.stopLock: Lock = Lock()
      self.endCondition: Condition = Condition(self.stopLock)
      self.stop: str = '' # the reason we stopped
      self.stop_rc: Optional[int] = None # the return code (int if done, None if still running)

   def waitForDoneReading(self, timeout=30) -> None:
      timeoutExpired = False
      self.pollForLock(self.doneReadingLock, timeout=timeout)
      if not self.doneReading:
         tmp = self.doneRecvCondition.wait(timeout=timeout)
         if not tmp:
            timeoutExpired = True
      self.doneReadingLock.release()
      if timeoutExpired:
         raise Exception('timeout expired waiting for for recv to end after proc stopped')
         
   def setDoneReading(self, val: bool) -> None:
      self.pollForLock(self.doneReadingLock, timeout=5)
      self.doneReading = val
      self.doneRecvCondition.notify_all()
      self.doneReadingLock.release()

   @property
   def pid(self) -> int:
      if not self.proc:
         return 0
      return self.proc.pid

   def __repr__(self) -> str:
      return str(self)

   def __str__(self) -> str:
      return '<ProcessHandler: pid={}, cmd={}, cwd="{}", stop_reason="{}", stop_rc={}>'.format(
         self.pid, self.command, self.cwd, self.stop, self.stop_rc)

   @property
   def isStopped(self) -> str:
      notify = False
      self.stopLock.acquire()
      if not self.stop and self.proc:
         self.stop_rc = self.proc.poll()
         if self.stop_rc != None:
            self.stop = 'Process ended with exit code: {}'.format(self.stop_rc)
            notify = True

      stopVal = self.stop
      self.stopLock.release()

      if notify:
         self.endCondition.acquire()
         self.endCondition.notify_all()
         self.endCondition.release()

      return stopVal

   def getStopInfo(self) -> Tuple[str, Optional[int]]:
      stop: str = ''
      stop_rc: Optional[int] = None

      self.stopLock.acquire()
      stop = self.stop
      stop_rc = self.stop_rc
      self.stopLock.release()

      return stop, stop_rc      

   def start(self) -> None:
      if isLinux:
         setsid = os.setsid
      else:
          setsid = None

      if isLinux:
          master, slave = pty.openpty()

          fdNonBlocking(master)
          fdNonBlocking(slave)

          tty.setraw(master, termios.TCSANOW)
          readInFd = slave # slave can read from us
          writeOutFd = slave # and write to us
          writeInFd = master # we can write to the slave
          readOutFd = master # and read from the slave
      else:
          readOutFd, writeOutFd = os.pipe() # pipes from process (writer) to us (reader)
          readInFd, writeInFd = os.pipe() # pipes from us (writer) to process (reader)
          if not fdNonBlocking(readOutFd):
             raise Exception('failed to set pipe fd to non-blocking')

      env = os.environ.copy()
      env['TERM'] = 'xterm-old'

      self.readOutFd = readOutFd # reading process output
      self.writeInFd = writeInFd # writing to process stdin
      self.proc = subprocess.Popen(self.command,
            bufsize=self.bufSize,
            stdin=readInFd,
            stdout=writeOutFd,
            stderr=writeOutFd,
            preexec_fn=setsid,
            cwd=self.cwd,
            env=env)

      # start recv thread
      self.recvThread = Thread(target=self.recvFn)
      self.recvThread.start()

   # return output, timeoutExpired, returnCode
   def waitForEnd(self, timeout: float=30) -> Tuple[bytes, bool, int]:
      timeoutExpired = False
      self.endCondition.acquire()
      if not self.stop:
         tmp = self.endCondition.wait(timeout=timeout)
         if not tmp:
            timeoutExpired = True
      self.endCondition.release()

      output, rc = self.end()

      return output, timeoutExpired, rc

   def end(self, basic: bool=False) -> Tuple[bytes, int]:
      self.stopLock.acquire()
      if not self.stop:
         # this will tell recv thread to exit
         self.stop = 'Forcing process to end with ProcessHandler.end()'
         if self.proc and self.stop_rc == None:
            self.stop_rc = -1 # must kill the process
      self.stopLock.release()      

      if self.recvThread and not basic:
         start = time.time()
         while self.recvThread.is_alive():
            self.recvThread.join(3)
            if time.time() - start > 30:
               self.error('unable to end ProcessHandler send/recv threads')
      
      if self.proc:
         if self.proc.poll() == None:
            self.proc.kill()

      # wake up any threads blocked on waitForEnd
      self.endCondition.acquire()
      self.endCondition.notify()
      self.endCondition.release()

      if basic:
         return b'', -1

      return self.recvAtEnd()

   def recvAtEnd(self) -> Tuple[bytes, int]:
      stop, stop_rc = self.getStopInfo()
      if stop_rc == None:
         stop_rc = -1

      self.pollForLock(self.recvBufferLock, timeout=5)
      output = self.recvBuffer
      self.recvBufferLock.release()

      return output, stop_rc

   def error(self, msg: str) -> None:
      try:
         self.stopLock.release()
      except:
         pass
      self.end(basic=True)
      raise ProcessHandlerException('{}: recv buffer = "{}"'.format(msg, bToStr(self.recvBuffer)), self.isStopped)

   def pollForLock(self, lock: Lock, timeout: float=5 * 60, interval: float=0.2, ignStop: bool = True) -> bool:
      start = time.time()
      while time.time() - start <= timeout:
         if lock.acquire(timeout=interval):
            return True
         if ignStop:
            continue
         stopped = self.isStopped
         if stopped:
            lock.release()
            self.error('failed to acquire lock since ProcessHandler is stopped: ' + stopped)
      self.error('failed to acquire lock within timeout of {} sec'.format(timeout))
      return False

   # regex is a string, we will convert to bytes
   # return output (string) along with bool if we found the regex
   def recv(self, regexStr: str, timeout: float=30) -> Tuple[bytes, bool, Optional['Match']]:
      oneChance = self.isStopped
      if oneChance:
         # wait for read buffer to finish
         self.waitForDoneReading()
      
      #if oneChance and not self.recvBuffer:
      #   self.error('recv failed since ProcessHandler is stopped: ' + str(oneChance))
      regex: bytes = regexStr.encode('ascii')
      received = b''
      start = time.time()
      while not received:
         if time.time() - start >= timeout:
            self.pollForLock(self.recvBufferLock, timeout=30)
            received = self.recvBuffer
            self.recvBuffer = b''
            self.recvBufferLock.release()
            return received, False, None
         err = None
         gotLock = False
         try:
            m = re.search(regex, self.recvBuffer, re.M)
            if m:
               # found the regex
               received = self.recvBuffer[:m.span()[1]]
               inc = 0
               self.pollForLock(self.recvBufferLock, timeout=30)
               gotLock = True
               self.recvBuffer = self.recvBuffer[m.span()[1]:]
               self.recvBufferLock.release()
               gotLock = False
               return received, True, m
         except:
            if gotLock:
               self.recvBufferLock.release()
            err = traceback.format_exc()
         if err:
            self.error(err)
         if oneChance:
            # nothing else will enter the buffer, so since we didn't find it,
            # we never will, and neither can anything else
            timeout = 0
      return received, False, None
 
   def send(self, dataStr: str) -> bool:
      stopped = self.isStopped
      if stopped:
         self.error('send failed since ProcessHandler is stopped: ' + stopped)

      data: bytes = strToBytes(dataStr)
      offset = 0
      dataLeft = len(data)
      start = time.time()

      while dataLeft > 0:
         try:
            nWrite = os.write(self.writeInFd, data[offset:])
         except OSError as e:
            if e.errno == 11:
               nWrite = 0
         offset += nWrite
         dataLeft -= nWrite
         time.sleep(0.1)
         if time.time() - start > 60*60:
            self.error('ProcessHandler.send timed out after 1 hr: {}'.format(traceback.format_exc()))

      return True

   def sanitizeAnsi(self, data: bytes) -> bytes:
      data = data.replace(b'\x1b[6n', b'')
      data = data.replace(b'\x1b[H', b'')
      data = data.replace(b'\x1b[J', b'')
      return data

   def recvFn(self) -> None:
      run = True
      while run:
         if self.isStopped:
            run = False # but we get one more try to read
         start = time.time()
         self.pollForLock(self.recvBufferLock, timeout=30)
         #print('got recv lock')
         try:
            nBuf = 0
            data = b''
            while nBuf < self.targetBufSize:
               try:
                  tmp = os.read(self.readOutFd, self.bufSize)
               except OSError as e:
                  if not isLinux:
                     if GetLastError() != ERROR_NO_DATA:
                        raise
                     else:
                        break
                  else:
                     break
               data += tmp
               if len(tmp) != self.bufSize:
                  break
               if self.isStopped:
                  break
               nBuf += self.bufSize
            #print('recv += ' + bToStr(data))
            self.recvBuffer += data
            self.recvBuffer = self.sanitizeAnsi(self.recvBuffer)
            self.recvBufferLock.release()
            #print('released recv lock')
         except:
            self.recvBufferLock.release()
            self.error(traceback.format_exc())
      self.setDoneReading(True)
