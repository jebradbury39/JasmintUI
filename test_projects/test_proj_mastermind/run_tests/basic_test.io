[OUT]
Enter the seed for random number generation: 
[IN]
5
[OUT]
Enter the maximum letter for the game (A-Z): 
[IN]
B
[OUT]
Enter the number of positions for the game (1-8): 
[IN]
2
[OUT]
Enter the number of guesses allowed for the game: 
[IN]
5
[OUT]
start game loop

Enter guess 1: 
[SCRIPT]
passing = False
for guess in ['AA', 'AB', 'BA', 'BB']:
   tsend(guess + '\n')
   out, _, m = trecv(r'(Wow, you won in \d+ guesses - well done!)|(Nope, \d+ exact guesses and \d+ inexact guesses\s+Enter guess \d+: )')
   if not m:
      raise Exception('failed to find expected output: "{}"'.format(out))
   if m.group(1):
      passing = True
      break
if not passing:
   raise Exception('failed to find win prompt after all 4 possible guesses')
[RC=0]
